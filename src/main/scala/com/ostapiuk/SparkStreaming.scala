package com.ostapiuk

import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.functions.{round, to_date, to_json}

object SparkStreaming {
  def main(args: Array[String]): Unit = {
    val spark = SparkSession
      .builder()
      .master("local[*]")
      .appName("Streaming")
      .getOrCreate()
    spark.sparkContext.setLogLevel("ERROR")
    import spark.implicits._

    val year2016 = spark.read.format("parquet")
      .load("src/main/resources/weather/year=2016")
    year2016.createOrReplaceTempView("year2016")

    val year2017 = spark.readStream.schema(Utils.schema2017).option("maxFilesPerTrigger", 1).format("parquet")
      .load("src/main/resources/weather/year=2017").select("*")
      .withColumn("wthr_date", to_date($"wthr_date", "yyyy-MM-dd"))
      .withColumn("avg_tmpr_f", round($"avg_tmpr_f", 2))
      .withColumn("avg_tmpr_c", round($"avg_tmpr_c", 2))
    year2017.createOrReplaceTempView("year2017")

    val hotel = spark.readStream.schema(Utils.hotelSchema).option("maxFilesPerTrigger", 1).format("csv")
      .load("src/main/resources/hotels").select("*")
    hotel.createOrReplaceTempView("hotels")

    val booking = spark.readStream.schema(Utils.bookingSchema).option("maxFilesPerTrigger", 1).format("csv")
      .load("src/main/resources/expedia").select("*")
      .withColumn("srch_ci", to_date($"srch_ci", "yyyy-MM-dd"))
      .withColumn("srch_co", to_date($"srch_co", "yyyy-MM-dd"))
    booking.createOrReplaceTempView("booking")

    val averageDay2016 = spark.sql("SELECT wthr_date, AVG(avg_tmpr_f) AS avg_day_temp_f, " +
      "AVG(avg_tmpr_c) AS avg_day_temp_c FROM year2016 GROUP BY wthr_date " +
      "HAVING avg_day_temp_c > 0 AND avg_day_temp_f > 32")
    averageDay2016.createOrReplaceTempView("averageDay2016")

    val averageDay2017 = spark.sql("SELECT wthr_date, AVG(avg_tmpr_f) AS avg_day_temp_f, " +
      "AVG(avg_tmpr_c) AS avg_day_temp_c FROM year2017 GROUP BY wthr_date " +
      "HAVING avg_day_temp_c > 0 AND avg_day_temp_f > 32")
      .withColumn("avg_day_temp_f", round($"avg_day_temp_f", 2))
      .withColumn("avg_day_temp_c", round($"avg_day_temp_c", 2))
    averageDay2017.createOrReplaceTempView("averageDay2017")

    val joinedData = spark.sql("SELECT DISTINCT b.srch_ci, avg_day_temp_f, a.avg_day_temp_c FROM booking b " +
      "INNER JOIN averageDay2017 a ON (b.srch_ci = a.wthr_date)")
    joinedData.createOrReplaceTempView("joined")

    val differenceBooking = spark.sql("SELECT hotel_id, date_time, srch_children_cnt, " +
      "srch_ci, srch_co, DATEDIFF(srch_co, srch_ci) AS DateDiff FROM booking")
    differenceBooking.createOrReplaceTempView("diffBooking")

    val stateLogicBooking = spark.sql("SELECT hotel_id, date_time, srch_children_cnt, " +
      "CASE WHEN srch_children_cnt >= 1 THEN 'With children' " +
      "ELSE 'Without children' END children_cnt, " +
      "srch_ci, srch_co, DateDiff, " +
      "CASE WHEN DateDiff >= 1 AND DateDiff <= 3 THEN 'Short stay' " +
      "WHEN DateDiff >= 4 AND DateDiff <= 7 THEN 'Standart stay' " +
      "WHEN DateDiff >= 8 AND DateDiff <= 14 THEN 'Standart extended stay' " +
      "WHEN DateDiff >= 15 AND DateDiff <= 30 THEN 'Long stay' " +
      "ELSE 'Erroneous data' END stayDaysCASE FROM diffBooking")
    stateLogicBooking.createOrReplaceTempView("stateBooking")

    val recordCountBooking = spark.sql("SELECT hotel_id, COUNT(CASE WHEN stayDaysCASE = 'Short stay' THEN 1 " +
      "ELSE null END) AS ShortCount, COUNT(CASE WHEN stayDaysCASE = 'Standart stay' THEN 1 ELSE null END) AS StandartCount, " +
      "COUNT(CASE WHEN stayDaysCASE = 'Standart extended stay' THEN 1 ELSE null END) AS StandartExtCount, " +
      "COUNT(CASE WHEN stayDaysCASE = 'Long stay' THEN 1 ELSE null END) AS LongCount, " +
      "COUNT(CASE WHEN stayDaysCASE = 'Erroneous data' THEN 1 ELSE null END) AS ErrorCount, " +
      "COUNT(stayDaysCASE) AS totalCount, " +
      "COUNT(CASE WHEN children_cnt = 'With children' THEN 1 ELSE null END) AS WithChildCount, " +
      "COUNT(CASE WHEN children_cnt = 'Without children' THEN 1 ELSE null END) AS WithoutChildCount, " +
      "COUNT(children_cnt) AS totalChildCount FROM stateBooking GROUP BY hotel_id")
    recordCountBooking.createOrReplaceTempView("countedBooking")

    val output = recordCountBooking.selectExpr("CAST(hotel_id AS STRING) AS key",
      "to_json(struct(*)) AS value")
      .as[(String, String)]

    //    val res = output.writeStream
    //      .outputMode("complete")
    //      .format("console")
    //      .start().awaitTermination()

    val topic = "Ostapiuk"
    val port = "localhost:9092"
    val writer = new Producer(topic, port)
    val result = output
      .writeStream
      .foreach(writer)
      .outputMode("update")
      .start()
      .awaitTermination()

    spark.stop()
  }
}

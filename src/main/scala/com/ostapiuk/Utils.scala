package com.ostapiuk

import org.apache.spark.sql.types.{DoubleType, IntegerType, StringType, StructField, StructType}

object Utils {
  val schema2017 = StructType(
    List(
      StructField("lng", DoubleType, nullable = true),
      StructField("lat", DoubleType, nullable = true),
      StructField("avg_tmpr_f", DoubleType, nullable = true),
      StructField("avg_tmpr_c", DoubleType, nullable = true),
      StructField("wthr_date", StringType, nullable = true),
      StructField("month", IntegerType, nullable = true),
      StructField("day", IntegerType, nullable = true)
    )
  )

  val hotelSchema = StructType(
    List(
      StructField("Id", IntegerType, nullable = true),
      StructField("Name", StringType, nullable = true),
      StructField("Country", StringType, nullable = true),
      StructField("City", StringType, nullable = true),
      StructField("Address", StringType, nullable = true),
      StructField("Latitude", StringType, nullable = true),
      StructField("Longitude", StringType, nullable = true)
    )
  )

  val bookingSchema = StructType(
    List(
      StructField("id", IntegerType, nullable = true),
      StructField("date_time", StringType, nullable = true),
      StructField("site_name", StringType, nullable = true),
      StructField("posa_continent", StringType, nullable = true),
      StructField("user_location_country", StringType, nullable = true),
      StructField("user_location_region", StringType, nullable = true),
      StructField("user_location_city", StringType, nullable = true),
      StructField("orig_destination_distance", StringType, nullable = true),
      StructField("user_id", StringType, nullable = true),
      StructField("is_mobile", StringType, nullable = true),
      StructField("is_package", StringType, nullable = true),
      StructField("channel", StringType, nullable = true),
      StructField("srch_ci", StringType, nullable = true),
      StructField("srch_co", StringType, nullable = true),
      StructField("srch_adults_cnt", IntegerType, nullable = true),
      StructField("srch_children_cnt", IntegerType, nullable = true),
      StructField("srch_rm_cnt", IntegerType, nullable = true),
      StructField("srch_destination_id", StringType, nullable = true),
      StructField("srch_destination_type_id", StringType, nullable = true),
      StructField("hotel_id", StringType, nullable = true)
    )
  )
}

name := "task06_SS"

version := "0.1"

libraryDependencies ++= Seq(
  "org.apache.spark" % "spark-core_2.12" % "2.4.4",
  "org.apache.spark" % "spark-avro_2.12" % "2.4.4",
  "org.apache.spark" % "spark-sql_2.12" % "2.4.4",
  "org.apache.spark" % "spark-sql-kafka-0-10_2.12" % "2.4.0",
  "org.apache.maven.plugins" % "maven-shade-plugin" % "3.2.1"
)
